import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine{
    private JPanel root;
    private JTextPane panel;
    private JTextArea orderlist;
    private JSpinner spinner1;
    private JButton ok1;
    private JSpinner spinner6;
    private JSpinner spinner5;
    private JSpinner spinner4;
    private JTextPane totalprice;
    private JButton checkout;
    private JSpinner spinner2;
    private JSpinner spinner3;
    private JButton ok2;
    private JButton ok4;
    private JButton ok5;
    private JButton ok6;
    private JButton reset;
    private JButton ok3;
    private JLabel fn2;
    private JLabel fn1;
    private JLabel fn3;
    private JLabel fn4;
    private JLabel fn5;
    private JLabel fn6;
    private JLabel Label1;
    private JLabel Label2;
    private JLabel Label3;
    private JLabel Label4;
    private JLabel Label5;
    private JLabel Label6;
    private int total=0;
    private int x=0;
    /*public void setImage(){
        ImageIcon icon = new ImageIcon(getClass().getResource("image/Nasi Lemak.jpg"));
        Image img = icon.getImage().getScaledInstance(Label1.getWidth(),Label1.getHeight(),Image.SCALE_SMOOTH);
    }*/
    void confirmation(String food,int qty,int fp){
        int confirm = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+qty+" of "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirm == 0){
            panel.setText(panel.getText()+"Preparing "+qty+" of "+food+".\n");
            JOptionPane.showMessageDialog(null, "Order for "+food+" received.");
            x++;
            int price=qty*fp;
            orderlist.setText(orderlist.getText()+x+". "+food+"\n(qtty:"+qty+"x"+"500)  "+price+"円\n");
            total+=price;
            totalprice.setText(total+" 円");
        }
    }
    public boolean qtyzero(int qty){
        if (qty==0){
            JOptionPane.showMessageDialog(null,
                    "Please insert the item quantity");
            return false;
        } else if (qty<0) {
            JOptionPane.showMessageDialog(null,
                    "Quantity cannot in negative.\nPlease select only positive number.");
            return false;
        }
        return true;
    }
    public void reset(){
        spinner1.setValue(0);
        spinner2.setValue(0);
        spinner3.setValue(0);
        spinner4.setValue(0);
        spinner5.setValue(0);
        spinner6.setValue(0);
        totalprice.setText("0.0");
        orderlist.setText("");
        panel.setText("");
        x=0;
        total=0;
    }
    public SimpleFoodOrderingMachine() {
        //setImage();
        reset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });
        ok1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int qty = Integer.parseInt(spinner1.getValue().toString());
                if (qtyzero(qty)){
                    confirmation(fn1.getText(),qty,500);
                }
                spinner1.setValue(0);
            }
        });
        ok2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int qty = Integer.parseInt(spinner2.getValue().toString());
                if (qtyzero(qty)){
                    confirmation(fn2.getText(),qty,580);
                }
                spinner2.setValue(0);
            }
        });
        ok3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int qty = Integer.parseInt(spinner3.getValue().toString());
                if (qtyzero(qty)){
                    confirmation(fn3.getText(),qty,680);
                }
                spinner3.setValue(0);
            }
        });
        ok4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int qty = Integer.parseInt(spinner4.getValue().toString());
                if (qtyzero(qty)){
                    confirmation(fn4.getText(),qty,730);
                }
                spinner4.setValue(0);
            }
        });
        ok5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int qty = Integer.parseInt(spinner5.getValue().toString());
                if (qtyzero(qty)){
                    confirmation(fn5.getText(),qty,390);
                }
                spinner5.setValue(0);
            }
        });
        ok6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int qty = Integer.parseInt(spinner6.getValue().toString());
                if (qtyzero(qty)){
                    confirmation(fn6.getText(),qty,540);
                }
                spinner6.setValue(0);
            }
        });
        checkout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(total==0.0){
                    JOptionPane.showMessageDialog(null,
                            "You haven't order any food yet.");
                }else {
                    int payment = JOptionPane.showConfirmDialog(
                            null,
                            "Please confirm your ordered item\n"+orderlist.getText()+"\nTotal Price is "+total+"円\nproceed to checkout?",
                            "Checkout",
                            JOptionPane.YES_NO_OPTION
                    );
                    if (payment == 0){
                        panel.setText("Thank you for visiting our restaurant.\nPlease Come again.");
                        JOptionPane.showMessageDialog(null, "Thank you for visiting our restaurant.\nPlease go to the cashier to complete your payment.");
                        reset();
                    }
                }
            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("Food Ordering Window");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
